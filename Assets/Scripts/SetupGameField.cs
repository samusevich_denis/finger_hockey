﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupGameField : MonoBehaviour
{
    [SerializeField] private Transform topFencing;
    [SerializeField] private Transform bottomFencing;
    [SerializeField] private Transform rightFencing;
    [SerializeField] private Transform leftFencing;
    [SerializeField] private Transform gate;
    [SerializeField] private Transform center;
    private Vector2 sizeColliderGate;
    private float offsetGateY = SettingGame.FieldOffsetGateY;
    private float ratioField;
    public static Vector3 FieldBoundaryDownLeft { get => fieldBoundaryDownLeft; private set => fieldBoundaryDownLeft = value; }
    public static Vector3 FieldBoundaryUpRight { get => fieldBoundaryUpRight; private set => fieldBoundaryUpRight = value; }
    private static Vector3 fieldBoundaryDownLeft;
    private static Vector3 fieldBoundaryUpRight;
    public static Vector3 StartPositionPuck { get; private set; }

    public void ActivationField()
    {
        Vector3 minPointScreen = GameController.CameraGame.ScreenToWorldPoint(Vector3.zero);
        if (minPointScreen.x>minPointScreen.y)
        {
            ratioField = minPointScreen.x/minPointScreen.y;
        }
        else{
            ratioField = minPointScreen.y/minPointScreen.x;
        }
        bottomFencing.position = new Vector3(0f, minPointScreen.y, 0f);
        rightFencing.position = new Vector3(Mathf.Abs(minPointScreen.y)*ratioField, 0f, 0f);
        leftFencing.position = new Vector3(minPointScreen.y*ratioField, 0f, 0f);
        gate.position = new Vector3(0f, Mathf.Abs(minPointScreen.y) - offsetGateY*ratioField, 0f);
        topFencing.position = new Vector3(0f, Mathf.Abs(minPointScreen.y) - offsetGateY*ratioField, 0f);
        center.position = new Vector3(0f,-offsetGateY*0.5f*ratioField,0f);
        GetFieldBoundary(out fieldBoundaryDownLeft, out fieldBoundaryUpRight);
        StartPositionPuck = new Vector3(0f, (center.position.y+ FieldBoundaryDownLeft.y)*0.5f, 0f);
        sizeColliderGate = gate.GetComponent<BoxCollider2D>().size;
        GameController.NextLevelAction += SetRandomPositionGate;
    }

    private void GetFieldBoundary(out Vector3 fieldBoundaryDownLeft, out Vector3 fieldBoundaryUpRight)
    {
        fieldBoundaryDownLeft = Vector3.zero;
        fieldBoundaryUpRight = Vector3.zero;
        var topCollider = topFencing.GetComponent<BoxCollider2D>();
        var bottomCollider = bottomFencing.GetComponent<BoxCollider2D>();
        var rightCollider = rightFencing.GetComponent<BoxCollider2D>();
        var leftCollider = leftFencing.GetComponent<BoxCollider2D>();
        fieldBoundaryUpRight.y = topFencing.position.y - topCollider.size.y * 0.5f;
        fieldBoundaryUpRight.x = rightFencing.position.x - rightCollider.size.x * 0.5f;
        fieldBoundaryDownLeft.y = bottomFencing.position.y + bottomCollider.size.y * 0.5f;
        fieldBoundaryDownLeft.x = leftFencing.position.x + leftCollider.size.x * 0.5f;
    }

    private void SetRandomPositionGate()
    {
        var position = gate.position;
        position.x = Random.Range(FieldBoundaryDownLeft.x + sizeColliderGate.x * 0.5f, FieldBoundaryUpRight.x - sizeColliderGate.x * 0.5f);
        gate.position = position;
    }

    private void OnDestroy()
    {
        GameController.NextLevelAction -= SetRandomPositionGate;
    }
}

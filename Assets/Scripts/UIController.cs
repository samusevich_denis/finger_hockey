﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private Text textLevel;
    public void Activation()
    {
        GameController.NextLevelAction += SetTextLevel;
    }
    
    private void SetTextLevel()
    {
        textLevel.text = GameController.CountLevel.ToString();
    }

    public void Exit()
    {
        Application.Quit();
    }

    private void OnDestroy()
    {
         GameController.NextLevelAction -= SetTextLevel;
    }
}

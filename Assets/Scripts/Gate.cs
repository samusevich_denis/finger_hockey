﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : EnterPuckForRestart
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (base.CheckPlayer(other))
        {
            GameController.NextLevel();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameController : MonoBehaviour
{
    public static event Action GameOverAction;
    public static event Action NextLevelAction;

    [SerializeField] private GameObject prefabField;
    [SerializeField] private GameObject prefabEnemyGenerator;
    [SerializeField] private GameObject prefabUIController;
    [SerializeField] private GameObject prefabMovePuck;
    private SetupGameField field;
    private EnemyGenerator enemyGenerator;
    private UIController uIController;
    private Puck puck;
    public static Camera CameraGame { get; private set; }
    public static int CountLevel { get; private set; }

    void Start()
    {
        CameraGame = Camera.main;
        var objField = Instantiate(prefabField);
        field = objField.GetComponent<SetupGameField>();
        var objEnemyGenerator = Instantiate(prefabEnemyGenerator);
        enemyGenerator = objEnemyGenerator.GetComponent<EnemyGenerator>();
        var objUIController = Instantiate(prefabUIController);
        uIController = objUIController.GetComponent<UIController>();
        var objMovePuck = Instantiate(prefabMovePuck);
        puck = objMovePuck.GetComponent<Puck>();
        field.ActivationField();
        enemyGenerator.Activation();
        uIController.Activation();
        puck.Activation();
        NextLevel();
    }

    public static void GameOver()
    {
        CountLevel = 1;
        GameOverAction?.Invoke();
        NextLevelAction?.Invoke();
    }

    public static void NextLevel()
    {
        CountLevel++;
        NextLevelAction?.Invoke();
    }

}

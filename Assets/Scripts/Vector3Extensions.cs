﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions
{
    public static Vector3 ClampVector3(this Vector3 outVector, Vector3 vector, Vector3 vectorMin, Vector3 vectorMax)
    {
        var x = Mathf.Clamp(vector.x, vectorMin.x, vectorMax.x);
        var y = Mathf.Clamp(vector.y, vectorMin.y, vectorMax.y);
        var z = Mathf.Clamp(vector.z, vectorMin.z, vectorMax.z);
        return new Vector3(x, y, z);
    }
    public static Vector3 GetRandomVector(this Vector3 Vector3, Vector3 vectorMin, Vector3 vectorMax)
    {
        var x = Random.Range(vectorMin.x, vectorMax.x);
        var y = Random.Range(vectorMin.y, vectorMax.y);
        var z = Random.Range(vectorMin.z, vectorMax.z);
        return new Vector3(x, y, z);
    }
}

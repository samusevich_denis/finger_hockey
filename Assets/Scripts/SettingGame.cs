﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SettingGame 
{
    public static float FieldOffsetGateY {get;set;} = 3f;
    public static float AdjustmentThrow {get;set;} = 0.5f;
}

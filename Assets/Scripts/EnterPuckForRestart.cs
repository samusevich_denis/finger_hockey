﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterPuckForRestart : MonoBehaviour
{
    protected bool CheckPlayer(Collider2D other)
    {
        if (other.transform != null && other.transform.TryGetComponent<Puck>(out Puck puck))
        {
            return true;
        }
        return false;
    }
}

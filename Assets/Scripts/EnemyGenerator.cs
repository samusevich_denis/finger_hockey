﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    [SerializeField] private GameObject prefabEnemyPuck;
    private List<Transform> transformEnemy;
    private Vector3 pointBoundaryDownLeftForEnemy;
    private Vector3 pointBoundaryUpRightForEnemy;
    private Vector3 halfSizePuck;
    public void Activation()
    {
        transformEnemy = new List<Transform>();
        pointBoundaryUpRightForEnemy = SetupGameField.FieldBoundaryUpRight;
        var pointDownLeft = SetupGameField.FieldBoundaryDownLeft;
        pointDownLeft.y = (pointBoundaryUpRightForEnemy.y+pointDownLeft.y)*0.5f;
        pointBoundaryDownLeftForEnemy = pointDownLeft;
        var obj = ObjectsPool.Instance.GetObject(prefabEnemyPuck);
        var collider = obj.GetComponent<CircleCollider2D>();
        float radius = collider.radius * transform.localScale.x;
        obj.SetActive(false);
        halfSizePuck = new Vector3(radius, radius, 0f)*obj.transform.localScale.x;
        GameController.NextLevelAction += LoadLevel;
    }

    private void LoadLevel()
    {
        for (int i = 0; i < transformEnemy.Count; i++)
        {
            transformEnemy[i].gameObject.SetActive(false);
        }
        transformEnemy.Clear();
        for (int i = 0; i < GameController.CountLevel / 2 + 2; i++)
        {
            var obj = ObjectsPool.Instance.GetObject(prefabEnemyPuck);
            obj.transform.position = obj.transform.position.GetRandomVector(pointBoundaryDownLeftForEnemy + halfSizePuck, pointBoundaryUpRightForEnemy - halfSizePuck); 
            while (CheckPosition(obj.transform.position))
            {
                obj.transform.position = obj.transform.position.GetRandomVector(pointBoundaryDownLeftForEnemy + halfSizePuck, pointBoundaryUpRightForEnemy - halfSizePuck); ;

            }
            transformEnemy.Add(obj.transform);
        }
    }

    private bool CheckPosition(Vector3 position)
    {
        for (int i = 0; i < transformEnemy.Count; i++)
        {
            if (Vector3.Distance(transformEnemy[i].position, position) < halfSizePuck.x * 2)
            {
                return true;
            }
        }
        return false;
    }
    
    private void OnDestroy()
    {
        GameController.NextLevelAction -= LoadLevel;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPuck : EnterPuckForRestart
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (CheckPlayer(other))
        {
            GameController.GameOver();
        }
    }
}

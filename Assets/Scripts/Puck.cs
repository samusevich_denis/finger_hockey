﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puck : MonoBehaviour
{
    private List<Vector3> MousePosition;
    private int countFrameForThrow = 2;
    private bool isDrag;
    private bool isMove;
    // private Vector3 offset;
    // private Vector3 vectorMaxPositionPuck;
    // private Vector3 vectorMinPositionPuck;
    private SpriteRenderer puckSprite;
    private Vector3 halfSizePuck;
    private Rigidbody2D rigidbody2DPuck;
    public void Activation()
    {
        MousePosition = new List<Vector3>();
        var collider = GetComponent<CircleCollider2D>();
        float radius = collider.radius * transform.localScale.x;
        halfSizePuck = new Vector3(radius, radius, 0f);
        rigidbody2DPuck = GetComponent<Rigidbody2D>();
        puckSprite = GetComponent<SpriteRenderer>();
        GameController.NextLevelAction += MoveOnStartPosition;
    }
    private void OnMouseDown()
    {
        if (isMove)
        {
            return;
        }
        isDrag = true;
        Debug.Log(isDrag);
        puckSprite.color = Color.blue;
        //offset = GameController.CameraGame.ScreenToWorldPoint(Input.mousePosition) - transform.position;
    }

    private void OnMouseDrag()
    {
        if (!isDrag)
        {
            return;
        }

        SaveMousePosition();
        // var position= transform.position;
        // position = position.ClampVector3(GameController.CameraGame.ScreenToWorldPoint(Input.mousePosition)-offset, SetupGameField.FieldBoundaryDownLeft + halfSizePuck, SetupGameField.FieldBoundaryUpRight - halfSizePuck);
        // transform.position =position;
    }

    private void OnMouseUp()
    {
        if (!isDrag)
        {
            return;
        }
        if (MousePosition.Count < 2)
        {
            return;
        }
        Vector3 directionThrow = MousePosition[1] - MousePosition[0];
        rigidbody2DPuck.velocity = directionThrow / Time.deltaTime * SettingGame.AdjustmentThrow;
        isMove = true;
        isDrag = false;
        puckSprite.color = Color.green;
    }

    void Update()
    {
        if (rigidbody2DPuck.velocity.sqrMagnitude < 0.0005f)
        {
            isMove = false;
        }
        if (isDrag || isMove)
        {
            return;
        }
        puckSprite.color = Color.white;
    }
    private void SaveMousePosition()
    {
        if (MousePosition.Count < countFrameForThrow)
        {
            MousePosition.Add(GameController.CameraGame.ScreenToWorldPoint(Input.mousePosition));
        }
        MousePosition.RemoveAt(0);
        MousePosition.Add(GameController.CameraGame.ScreenToWorldPoint(Input.mousePosition));
    }
    private void MoveOnStartPosition()
    {
        rigidbody2DPuck.velocity = Vector2.zero;
        transform.position = SetupGameField.StartPositionPuck;
    }
    private void OnDestroy()
    {
        GameController.NextLevelAction -= MoveOnStartPosition;
    }
}
